[![pipeline status](https://gitlab.com/npierreyves/kata_bank/badges/main/pipeline.svg)](https://gitlab.com/npierreyves/kata_bank/-/commits/main) [![coverage report](https://gitlab.com/npierreyves/kata_bank/badges/main/coverage.svg)](https://gitlab.com/npierreyves/kata_bank/-/commits/main)

# kata_bank

Bank account kata Think of your personal bank account experience When in doubt, go for the simplest solution
Requirements

1. Deposit and Withdrawal
2. Account statement (date, amount, balance)
3. Statement printing

## User Stories

### US #1 : _In order to save money_

- **As a** bank client
- **I want** to make a deposit in my account

### US #2 : _In order to retrieve some or all of my savings_

- **As a** bank client
- **I want** to make a withdrawal from my account

### US #3 : _In order to check my operations_

- **As a** bank client
- **I want** to see the history (operation, date, amount, balance) of my operations

## TDD

JaCoCO code coverage report : [REPORT](https://npierreyves.gitlab.io/kata_bank/jacoco/).

