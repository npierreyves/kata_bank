package fr.pyn.kata.domain;

import lombok.Data;

import java.io.PrintStream;

@Data
public class StatementLine {

    private final Transaction transaction;
    private final Amount currentBalance;

    public void printTo(PrintStream printer) {
        transaction.printTo(printer, currentBalance);
    }
}
