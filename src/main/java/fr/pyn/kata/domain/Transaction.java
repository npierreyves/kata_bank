package fr.pyn.kata.domain;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class Transaction {

    private final Amount value;
    private final LocalDateTime dateTime;

    public Amount balanceAfterTransaction(Amount balance) {
        return balance.operation(value);
    }

    public void printTo(PrintStream printer, Amount currentBalance) {
        String date = StringUtils.rightPad(dateTime.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        String credit;
        String debit;
        if (value.isGreaterThan(Amount.amountOf(0F))) {
            credit = StringUtils.leftPad(value.toString(), 10);
            debit = StringUtils.leftPad("", 10);
        } else {
            credit = StringUtils.leftPad("", 10);
            debit = StringUtils.leftPad(value.toString(), 10);
        }

        String balance = StringUtils.leftPad(currentBalance.toString(), 10);

        printer.println(String.format("%s | %s | %s | %s", date, credit, debit, balance));
    }
}
