package fr.pyn.kata.domain;

import lombok.Data;

import java.io.PrintStream;
import java.time.LocalDateTime;

@Data
public class Account {

    private final Statement statement;
    private Amount balance = Amount.amountOf(0F);

    public void deposit(Amount value, LocalDateTime date) {
        recordTransaction(value, date);
    }

    public void withdrawal(Amount value, LocalDateTime date) {
        recordTransaction(value.negative(), date);
    }

    public void printStatement(PrintStream printer) {
        statement.printTo(printer);
    }

    private void recordTransaction(Amount value, LocalDateTime date) {
        Transaction transaction = new Transaction(value, date);
        Amount balanceAfterTransaction = transaction.balanceAfterTransaction(balance);
        balance = balanceAfterTransaction;
        statement.addLineContaining(transaction, balanceAfterTransaction);
    }

}
