package fr.pyn.kata.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.text.DecimalFormat;

@RequiredArgsConstructor(staticName = "amountOf")
@Getter
@EqualsAndHashCode
public class Amount {
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.00");

    private final Float value;

    public Amount operation(Amount amount) {
        return amountOf(value + amount.value);
    }

    @Override
    public String toString() {
        return DECIMAL_FORMAT.format(value);
    }

    public boolean isGreaterThan(Amount otherAmount) {
        return this.value > otherAmount.value;
    }

    public Amount negative() {
        return amountOf(-value);
    }
}
