package fr.pyn.kata.domain;

import lombok.Data;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

@Data
public class Statement {
    public static final String STATEMENT_HEADER = "                      DATE|      CREDIT|       DEBIT|    BALANCE";

    private List<StatementLine> statementLines = new LinkedList<>();

    public void addLineContaining(Transaction transaction, Amount currentBalance) {
        statementLines.add(0, new StatementLine(transaction, currentBalance));
    }

    public void printTo(PrintStream printer) {
        printer.println(STATEMENT_HEADER);
        printStatementLines(printer);
    }

    private void printStatementLines(PrintStream printer) {
        for (StatementLine statementLine : statementLines) {
            statementLine.printTo(printer);
        }
    }
}
