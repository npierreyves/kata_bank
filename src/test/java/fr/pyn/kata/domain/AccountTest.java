package fr.pyn.kata.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;
import java.time.LocalDateTime;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountTest {

    @Mock
    private Statement statement;
    private Account account;

    @Before
    public void initialise() {
        account = new Account(statement);
    }

    @Test
    public void add_deposit_line_to_statement() {
        LocalDateTime depositDate = LocalDateTime.now();
        Amount depositAmount = Amount.amountOf(1000F);

        account.deposit(depositAmount, depositDate);

        Transaction transaction = new Transaction(depositAmount, depositDate);
        verify(statement).addLineContaining(transaction, depositAmount);
    }

    @Test
    public void add_withdrawal_line_to_statement() {
        LocalDateTime withdrawalDate = LocalDateTime.now();
        Amount withdrawalAmount = Amount.amountOf(500F);

        account.withdrawal(withdrawalAmount, withdrawalDate);

        Transaction transaction = new Transaction(withdrawalAmount.negative(), withdrawalDate);
        verify(statement).addLineContaining(transaction, withdrawalAmount.negative());
    }

    @Test
    public void print_statement() {
        PrintStream printer = System.out;

        account.printStatement(printer);

        verify(statement).printTo(printer);
    }
}
