package fr.pyn.kata.domain;

import org.junit.Assert;
import org.junit.Test;

public class AmountTest {

    @Test
    public void static_constructor() {
        Amount amount100 = Amount.amountOf(100F);

        Assert.assertEquals(new Float(100), amount100.getValue());
    }

    @Test
    public void equals() {
        Amount amount100 = Amount.amountOf(100F);
        Amount amount100Bis = Amount.amountOf(100F);

        Assert.assertEquals(amount100, amount100Bis);
    }

    @Test
    public void not_equals() {
        Amount amount100 = Amount.amountOf(100F);
        Amount amount50 = Amount.amountOf(50F);

        Assert.assertNotEquals(amount100, amount50);
    }

    @Test
    public void operation() {
        Amount amount100 = Amount.amountOf(100F);
        Amount amount50 = Amount.amountOf(50F);

        Amount resultOperation = Amount.amountOf(150F);

        Assert.assertEquals(resultOperation, amount100.operation(amount50));
    }

    @Test
    public void to_string() {
        Amount amount123 = Amount.amountOf(123F);

        Assert.assertEquals("123,00", amount123.toString());
    }

    @Test
    public void greater_than() {
        Amount amount10 = Amount.amountOf(10F);
        Amount amount5 = Amount.amountOf(5F);

        Assert.assertTrue(amount10.isGreaterThan(amount5));
    }

    @Test
    public void not_greater_than() {
        Amount amount10 = Amount.amountOf(10F);
        Amount amount5 = Amount.amountOf(5F);

        Assert.assertTrue(amount10.isGreaterThan(amount5));
    }
}
