package fr.pyn.kata.domain;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TransactionTest {

    @Mock
    PrintStream printer;

    @Test
    public void print_credit_transaction() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Transaction transaction = new Transaction(Amount.amountOf(1550F), now);
        transaction.printTo(printer, Amount.amountOf(500F));

        verify(printer).println(expectedDate + " |    1550,00 |            |     500,00");
    }

    @Test
    public void print_debit_transaction() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Transaction transaction = new Transaction(Amount.amountOf(-1550F), now);
        transaction.printTo(printer, Amount.amountOf(-50F));

        verify(printer).println(expectedDate + " |            |   -1550,00 |     -50,00");
    }

    @Test
    public void balance_after_deposit() {
        Transaction transaction = new Transaction(Amount.amountOf(1550F), LocalDateTime.now());
        Amount currentValue = transaction.balanceAfterTransaction(Amount.amountOf(555F));

        Assert.assertEquals(Amount.amountOf(2105F), currentValue);
    }

    @Test
    public void balance_after_withdrawal() {
        Transaction transaction = new Transaction(Amount.amountOf(-1550F), LocalDateTime.now());
        Amount currentValue = transaction.balanceAfterTransaction(Amount.amountOf(555F));

        Assert.assertEquals(Amount.amountOf(-995F), currentValue);
    }

    @Test
    public void equals() {
        LocalDateTime depositDate = LocalDateTime.now();
        Transaction transaction = new Transaction(Amount.amountOf(1000F), depositDate);
        Transaction anotherTransaction = new Transaction(Amount.amountOf(1000F), depositDate);

        Assert.assertEquals(transaction, anotherTransaction);
    }
}
