package fr.pyn.kata.domain;


import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StatementTest {

    @Mock
    PrintStream printer;

    @Mock
    Transaction transaction;

    private Statement statement;

    @Before
    public void initialise() {
        statement = new Statement();
    }

    @Test
    public void print_statement_header() {
        statement.printTo(printer);

        verify(printer).println(Statement.STATEMENT_HEADER);
    }

    @Test
    public void print_deposit() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Amount balance = Amount.amountOf(5000F);
        Transaction transaction = new Transaction(Amount.amountOf(100F), now);


        statement.addLineContaining(transaction, balance);

        statement.printTo(printer);

        verify(printer).println("                      DATE|      CREDIT|       DEBIT|    BALANCE");
        verify(printer).println(expectedDate + " |     100,00 |            |    5000,00");
    }

    @Test
    public void print_withdrawal() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Amount balance = Amount.amountOf(1.50F);
        Transaction transaction = new Transaction(Amount.amountOf(-650F), now);

        statement.addLineContaining(transaction, balance);

        statement.printTo(printer);

        verify(printer).println("                      DATE|      CREDIT|       DEBIT|    BALANCE");
        verify(printer).println(expectedDate + " |            |    -650,00 |       1,50");
    }

    @Test
    public void print_two_deposits_in_reverse_order() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Transaction transaction1 = new Transaction(Amount.amountOf(-250F), now);
        statement.addLineContaining(transaction1, Amount.amountOf(1000F));

        Transaction transaction2 = new Transaction(Amount.amountOf(2000F), now);
        statement.addLineContaining(transaction2, Amount.amountOf(750F));

        statement.printTo(printer);

        InOrder inOrder = Mockito.inOrder(printer);
        inOrder.verify(printer).println("                      DATE|      CREDIT|       DEBIT|    BALANCE");
        inOrder.verify(printer).println(expectedDate + " |    2000,00 |            |     750,00");
        inOrder.verify(printer).println(expectedDate + " |            |    -250,00 |    1000,00");

    }
}
