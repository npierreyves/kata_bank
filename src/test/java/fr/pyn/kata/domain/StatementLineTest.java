package fr.pyn.kata.domain;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StatementLineTest {

    @Mock
    PrintStream printer;

    @Test
    public void print_credit() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Amount balance = Amount.amountOf(5050F);
        Transaction transaction = new Transaction(Amount.amountOf(1250F), now);

        StatementLine statementLine = new StatementLine(transaction, balance);

        statementLine.printTo(printer);

        verify(printer).println(expectedDate + " |    1250,00 |            |    5050,00");
    }

    @Test
    public void print_withdrawal() {
        LocalDateTime now = LocalDateTime.now();
        String expectedDate = StringUtils.rightPad(now.format(DateTimeFormatter.ISO_DATE_TIME), 25);

        Amount balance = Amount.amountOf(1.50F);
        Transaction transaction = new Transaction(Amount.amountOf(-750F), now);

        StatementLine statementLine = new StatementLine(transaction, balance);

        statementLine.printTo(printer);

        verify(printer).println(expectedDate + " |            |    -750,00 |       1,50");
    }
}
